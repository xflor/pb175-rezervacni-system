from typing import List, Dict
import hashlib
import string
import random
import json
import datetime
import os


# type aliases
EventID = int
UserID = int
PlaceID = int
Place = Dict[str, str]
User = Dict[str, str]
Event = Dict[str, str]

# Global constants
SALT_LENGTH: int = 6

DATA_FILEPATH: str = "data"
PLACES_FILEPATH: str = DATA_FILEPATH + os.path.sep + "places.json"
ACCOUNTS_FILEPATH: str = DATA_FILEPATH + os.path.sep + "accs.json"
EVENTS_FILEPATH: str = DATA_FILEPATH + os.path.sep + "events.json"
RESERVATIONS_FILEPATH: str = DATA_FILEPATH + os.path.sep + "reservations.json"

# Global variables
user_id_counter = 1
event_id_counter = 1
place_id_counter = 1

places: Dict[PlaceID, Place] = {}
users: Dict[UserID, User] = {}
# hold Login and corresponding id
logins: Dict[str, UserID] = {}
events: Dict[EventID, Event] = {}

reservations_user: Dict[UserID, List[EventID]] = {}
reservations_event: Dict[EventID, List[UserID]] = {}


def set_logins() -> None:
    """ load logins from database to memory (dictionary)"""
    global logins

    for userid, user in users.items():
        logins[user.get("login")] = userid


def set_dicts() -> None:
    """load all the data from database to memory (dictionaries)"""
    global events, users, places, reservations_event, reservations_user

    events = load_json_file(EVENTS_FILEPATH)
    users = load_json_file(ACCOUNTS_FILEPATH)
    reservations_event = load_json_file(RESERVATIONS_FILEPATH)
    for user in users.keys():
        reservations_user[user] = []
    for event, usersid in reservations_event.items():
        for user in usersid:
            reservations_user[user].append(event)
    places = load_json_file(PLACES_FILEPATH)


def set_counter() -> None:
    """set counter variables based on last IDs from database"""
    global user_id_counter, event_id_counter, place_id_counter

    if users.keys():
        user_id_counter = int(max(users.keys())) + 1

    if events.keys():
        event_id_counter = int(max(events.keys())) + 1

    if places.keys():
        place_id_counter = int(max(places.keys())) + 1


def initialize() -> None:
    """initialize current session memory"""
    set_dicts()
    set_counter()
    set_logins()


def hash_passwd(passwd: str, salt: str) -> hex:
    """hash password concatenated with salt, return hex digest """
    m: hashlib.sha256 = hashlib.sha256()
    m.update(bytes(passwd, "utf-8"))
    m.update(bytes(salt, "utf-8"))
    return m.hexdigest()


def register(firstname: str, surname: str, login: str, passwd: str) -> int:
    """Register a user
    Keyword arguments:
        firstname -- the first name of the user
        surname -- the surname of the user
        login -- desired login of the user
        passwd -- password of the user
    Return values:
        -1 -- if firstname error
        -2 -- if login error
        -3 -- if login taken
        -4 -- if password error
    """
    # check if only alphabet chars, however this could/ should be done in GUI
    # to show alert
    if not firstname.isalpha() or 1 > len(firstname) > 20 or 1 > len(surname)\
            > 20 or not surname.isalpha():
        return -1
    # check if only alphanumeric chars, again....
    if not login.isalnum() or 1 > len(login) > 20:
        return -2
    # check whether login not already taken
    if login in logins:
        return -3
    # check if password includes only utf-8 characters, again....
    if not len(passwd) > 6:
        return -4
    try:
        passwd.encode('utf-8')
    except UnicodeError:
        return -4
    # create salt
    salt: str = "".join(random.choices(string.ascii_letters + string.digits,
                                       k=SALT_LENGTH))
    # hash password
    hashedpasswd: bytes = hash_passwd(passwd, salt)
    user: User = {"firstname": firstname, "surname": surname, "login": login,
                  "password_hash": hashedpasswd,
                  "salt": salt}
    global user_id_counter
    users[user_id_counter] = user
    logins[user.get("login")] = user_id_counter
    user_id_counter += 1
    update_json_file(users, ACCOUNTS_FILEPATH)
    return 0


def keystoint(x):
    """hook function for json loading, turn the string key IDs into ints"""
    result = {}
    for k, v in x:
        if k.isnumeric():
            result[int(k)] = v
        else:
            result[k] = v
    return result


def load_json_file(filepath: str) -> dict:
    """loads data from json file into a dictionary and returns it"""
    with open(filepath, "r", encoding="utf8") as f:
        record = json.load(f, object_pairs_hook=keystoint)
        return record


def update_json_file(record: dict, filepath: str) -> None:
    """Update record in corresponding json file"""
    with open(filepath, "w", encoding="utf8") as f:
        # default sets in which format to store object which aren't
        # serializable, ensure ascii false to allow utf chars
        json.dump(record, f, indent=4, default=str, ensure_ascii=False)


def create_reservation(userid: int, eventid: int) -> bool:
    """create a new reservation and add it to the database
    Keyword arguments:
        userid -- id of the user who to reserve
        eventid -- id of the event to reserve a spot at
    Return values:
        True -- successfully created reservation
        False -- error occurred
    """
    if userid not in users:
        return False
    if eventid not in events:
        return False
    # check if reservation already exists
    if reservations_event.get(eventid) is not None\
            and userid in reservations_event.get(eventid):
        return False
    user_add_reservation(userid, eventid)
    event_add_reservation(userid, eventid)
    update_json_file(reservations_event, RESERVATIONS_FILEPATH)

    events.get(eventid)["attending"] = events.get(eventid)["attending"] + 1
    update_json_file(events, EVENTS_FILEPATH)
    return True


def user_add_reservation(userid: int, eventid: int) -> None:
    """update dictionary with reservations for each user"""
    if reservations_user.get(userid) is None:
        reservations_user[userid] = [userid]
    else:
        reservations_user[userid].append(eventid)


def event_add_reservation(userid: int, eventid: int) -> None:
    """update dictionary with reservations for each event"""
    if reservations_event.get(eventid) is None:
        reservations_event[eventid] = [userid]
    else:
        reservations_event[eventid].append(userid)


def create_place(placeinfo: (str, str, str)) -> int:
    """create a place, check inputs and add it to the database
    Keyword arguments:
        placeinfo -- tuple with street, city, zipcode in that order
    Return values:
        -3 -- if zipcode format incorrect
        ID of the created place otherwise
    """
    street, city, zipcode = placeinfo
    if len(zipcode) != 5 or not zipcode.isnumeric():
        return -3

    place = {"street": street, "city": city, "zipcode": int(zipcode)}
    places[place_id_counter] = place
    update_json_file(places, PLACES_FILEPATH)
    return place_id_counter


def create_event(name: str, description: str, date: datetime.date,
                 capacity: int, place: (str, str, str)) -> int:
    """create an event, check inputs and add it to the database
    Keyword arguments:
        name -- name of the event
        description -- description of the event
        date -- date object of the date
        capacity -- maximum capacity of the event
        place -- tuple with street, city, zipcode in that order
    Return values:
        -1 -- if name length invalid
        -3 -- if place invalid
        -4 -- if capacity invalid
        -5 -- if past date
        0 -- on success
    """

    if 1 > len(name) > 40:
        return -1
    placeid = create_place(place)
    # if error in create_place, propagate error code
    if placeid < 0:
        return placeid
    # check if correct capacity
    if not 0 < capacity < 9999:
        return -4
    # check if correct date (not in the past), valid format should be
    # covered by gui date input
    if date < datetime.date.today():
        return -5
    date = datetime.datetime.strftime(date, '%d.%m.%Y')
    event: Event = {"name": name, "description": description, "date": date,
                    "capacity": capacity, "attending": 0,
                    "placeid": placeid,
                    "validity": True}
    global event_id_counter
    global place_id_counter
    events[event_id_counter] = event
    reservations_event[event_id_counter] = []
    event_id_counter += 1
    place_id_counter += 1
    update_json_file(events, EVENTS_FILEPATH)
    update_json_file(reservations_event, RESERVATIONS_FILEPATH)
    return 0


def remove_event(eventid: int) -> bool:
    """remove event from the database, return True if success,
     False otherwise"""
    if eventid not in events:
        return False
    # pop (remove) the reservation, then remove from list of
    # every user that is in reservation
    for userid in reservations_event.pop(eventid, []):
        reservations_user[userid].remove(eventid)
    # update database
    update_json_file(reservations_event, RESERVATIONS_FILEPATH)

    del events[eventid]
    update_json_file(events, EVENTS_FILEPATH)

    return True


def remove_reservation(userid: int, eventid: int) -> bool:
    """remove reservation from the database, return True if success,
     False otherwise"""
    if userid not in users:
        return False

    if eventid not in reservations_user.get(userid):
        return False

    reservations_event.get(eventid).remove(userid)
    reservations_user.get(userid).remove(eventid)
    update_json_file(reservations_event, RESERVATIONS_FILEPATH)

    events.get(eventid)["attending"] = events.get(eventid)["attending"] - 1
    update_json_file(events, EVENTS_FILEPATH)
    return True



def authenticate(login: str, passwd: str) -> bool:
    """authenticate the user, returns True on success, False otherwise"""
    if not login.isalnum():
        return False
    userid: int = logins.get(login)
    if userid is None:
        return False
    user = users.get(userid)
    return user.get("password_hash") == hash_passwd(passwd, user.get("salt"))


def get_user_reservations(userid: int) -> List[EventID]:
    """return list of events that the user has reservations for"""
    if userid not in reservations_user:
        return []
    return reservations_user[userid]


def get_user_firstname(userid: int) -> str:
    user = users.get(userid)
    return user.get("firstname")


def get_user_lastname(userid: int) -> str:
    user = users.get(userid)
    return user.get("surname")


def reset_database() -> None:
    """resets the database (empties all the database files)"""
    update_json_file({}, EVENTS_FILEPATH)
    update_json_file({}, RESERVATIONS_FILEPATH)
    update_json_file({}, PLACES_FILEPATH)
    update_json_file({}, ACCOUNTS_FILEPATH)
