import tkinter as tk
from tkinter import font
import tkcalendar
import functions
import datetime
import textwrap
from functools import partial
from tkinter import scrolledtext

CURRENT_USER = 1

functions.initialize()
window = tk.Tk()
window.geometry("800x600")


def set_screen(func):
    widgets = window.winfo_children()

    for label in window.grid_slaves():
        label.grid_forget()

    for item in widgets:
        if item.winfo_children():
            widgets.extend(item.winfo_children())
    for item in widgets:
        if type(item) == tk.Toplevel:
            item.destroy()
            continue
        item.destroy()

    func()


def screen_intro():
    for i in range(5):
        window.columnconfigure(i, weight=1, minsize=75)
        window.rowconfigure(i, weight=1, minsize=50)
        if i == 2:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5, sticky="ns")
                if j == 1:
                    btn = tk.Button(master=frame, text="Registrovat se",
                                    command=lambda:
                                    set_screen(screen_register))
                    btn.pack()
                if j == 3:
                    btn = tk.Button(master=frame, text=" Přihlásit se ",
                                    command=lambda:
                                    set_screen(screen_login))
                    btn.pack()
                else:
                    label = tk.Label(master=frame)
                    label.pack(padx=5, pady=5)

        elif i == 0:
            for j in range(5):
                frame = tk.Frame(master=window)
                frame.grid(row=i, column=j)
                if j == 2:
                    label = tk.Label(master=frame, text="  Rezervační Systém",
                                     font=("Arial", 15))
                    label.pack()
        else:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5)

                label = tk.Label(master=frame)
                label.pack(padx=5, pady=5)


def screen_register():
    frame_padding = tk.Frame(window)
    frame_padding.pack(pady=50)

    label = tk.Label(master=window, text="Založte si účet do systému",
                     font=("Arial", 15))
    label.pack(pady=20)

    def handle_focus_in(entry):
        entry.delete(0, tk.END)

    def handle_focus_in_pswd(entry):
        entry.delete(0, tk.END)
        entry.config(show="*")

    entry_jmeno = tk.Entry(window, text="Jméno", width=25)
    entry_jmeno.delete(0, tk.END)
    entry_jmeno.insert(0, "Jméno")
    entry_jmeno.bind("<FocusIn>", lambda event: handle_focus_in(entry_jmeno))

    entry_prijmeni = tk.Entry(window, text="Příjmení", width=25)
    entry_prijmeni.delete(0, tk.END)
    entry_prijmeni.insert(0, "Příjmení")
    entry_prijmeni.bind("<FocusIn>", lambda event:
                        handle_focus_in(entry_prijmeni))

    entry_login = tk.Entry(window, text="Uživatelské jméno", width=25)
    entry_login.delete(0, tk.END)
    entry_login.insert(0, "Uživatelské jméno")
    entry_login.bind("<FocusIn>", lambda event: handle_focus_in(entry_login))

    entry_heslo = tk.Entry(window, text="Heslo", width=25)
    entry_heslo.delete(0, tk.END)
    entry_heslo.insert(0, "Heslo")
    entry_heslo.bind("<FocusIn>", lambda event: handle_focus_in_pswd(entry_heslo))

    error = tk.StringVar()
    error.set("")
    label_error = tk.Label(window, textvariable=error, fg="red", width=50)

    entry_jmeno.pack(pady=15)
    entry_prijmeni.pack(pady=15)
    entry_login.pack(pady=15)
    entry_heslo.pack(pady=15)
    label_error.pack(pady=15)

    frame = tk.Frame(window)

    btn_back = tk.Button(frame, text="Zpět",
                         command=lambda: set_screen(screen_intro))

    def register():
        if 0 != functions.register(entry_jmeno.get(), entry_prijmeni.get(),
                                  entry_login.get(), entry_heslo.get()):
            error.set("Něco se pokazilo:( Uživatelské jméno je nejspíš zabrané")
        else:
            global CURRENT_USER
            CURRENT_USER = functions.logins.get(entry_login.get())
            set_screen(screen_user)

    btn_do = tk.Button(frame, text="Registrovat", command=register)

    btn_back.pack(side=tk.LEFT, padx=70, anchor=tk.CENTER)
    btn_do.pack(side=tk.LEFT, padx=70, fill="x")

    frame.pack(pady=20)


def screen_login():
    frame_padding = tk.Frame(window)
    frame_padding.pack(pady=50)

    label = tk.Label(master=window, text="Zadejte přihlašovací údaje",
                     font=("Arial", 15))
    label.pack(pady=20)

    def handle_focus_in(entry):
        entry.delete(0, tk.END)

    def handle_focus_in_pswd(entry):
        entry.delete(0, tk.END)
        entry.config(show="*")

    entry_jmeno = tk.Frame(window, width=25)

    error = tk.StringVar()
    error.set("")
    label_error = tk.Label(window, textvariable=error, fg="red", width=25)

    entry_login = tk.Entry(window, text="Uživatelské jméno", width=25)
    entry_login.delete(0, tk.END)
    entry_login.insert(0, "Uživatelské jméno")
    entry_login.bind("<FocusIn>", lambda event: handle_focus_in(entry_login))

    entry_heslo = tk.Entry(window, text="Heslo", width=25)
    entry_heslo.delete(0, tk.END)
    entry_heslo.insert(0, "Heslo")
    entry_heslo.bind("<FocusIn>", lambda event: handle_focus_in_pswd(entry_heslo))

    entry_login.pack(pady=15)
    entry_heslo.pack(pady=15)
    entry_jmeno.pack(pady=23)
    label_error.pack(pady=23)

    frame = tk.Frame(window)

    btn_back = tk.Button(frame, text="Zpět",
                         command=lambda: set_screen(screen_intro))

    def login():
        if not functions.authenticate(entry_login.get(), entry_heslo.get()):
            error.set("Nesprávný login/heslo!")
        else:
            global CURRENT_USER
            CURRENT_USER = functions.logins.get(entry_login.get())
            if CURRENT_USER == 1:
                set_screen(screen_admin)
            else:
                set_screen(screen_user)

    btn_do = tk.Button(frame, text="Přihlásit se", command=login)

    btn_back.pack(side=tk.LEFT, padx=70, anchor=tk.CENTER)
    btn_do.pack(side=tk.LEFT, padx=70, fill="x")

    frame.pack(pady=20)


def logout(event):
    global CURRENT_USER
    CURRENT_USER = 0
    set_screen(screen_intro)


def screen_user():
    for i in range(5):
        window.columnconfigure(i, weight=1, minsize=75)
        window.rowconfigure(i, weight=1, minsize=50)
        if i == 2:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5, sticky="ns")
                if j == 1:
                    btn = tk.Button(master=frame,
                                    text="  Spravovat rezervace   ",
                                    command=lambda:
                                    set_screen(screen_reservations))
                    btn.pack()
                if j == 3:
                    btn = tk.Button(master=frame,
                                    text="Vytvořit novou rezervaci",
                                    command=lambda:
                                    set_screen(screen_create_reservation))
                    btn.pack()
                else:
                    label = tk.Label(master=frame)
                    label.pack(padx=5, pady=5)

        elif i == 0:
            for j in range(5):
                frame = tk.Frame(master=window)
                frame.grid(row=i, column=j)
                if j == 0:
                    frame.grid(row=i, column=j, sticky="NW")
                    label = tk.Label(frame, text="odhlásit se",
                                     fg="dodger blue")
                    label.pack()
                    f = font.Font(label, label.cget("font"))
                    f.configure(underline=True)
                    label.configure(font=f)
                    label.bind("<Button-1>", logout)

                if j == 2:
                    label = tk.Label(master=frame, text="  Rezervační Systém",
                                     font=("Arial", 15))
                    label.pack()
                if j == 4:
                    frame.grid(row=i, column=j, sticky="NE")
                    label = tk.Label(frame,
                                     text=f"Přihlášen: "
                                          f"{functions.get_user_firstname(CURRENT_USER)} "
                                          f"{functions.get_user_lastname(CURRENT_USER)}")
                    label.pack()
        else:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5)

                label = tk.Label(master=frame)
                label.pack(padx=5, pady=5)


def screen_admin():
    for i in range(5):
        window.columnconfigure(i, weight=1, minsize=75)
        window.rowconfigure(i, weight=1, minsize=50)
        if i == 2:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5, sticky="ns")
                if j == 1:
                    btn = tk.Button(master=frame,
                                    text="  Spravovat události   ",
                                    command=lambda:
                                    set_screen(screen_reservations))
                    btn.pack()
                if j == 3:
                    btn = tk.Button(master=frame,
                                    text="Vytvořit novou událost",
                                    command=lambda:
                                    set_screen(screen_create_event))
                    btn.pack()
                else:
                    label = tk.Label(master=frame)
                    label.pack(padx=5, pady=5)

        elif i == 0:
            for j in range(5):
                frame = tk.Frame(master=window)
                frame.grid(row=i, column=j)
                if j == 0:
                    frame.grid(row=i, column=j, sticky="NW")
                    label = tk.Label(frame, text="odhlásit se",
                                     fg="dodger blue")
                    label.pack()
                    f = font.Font(label, label.cget("font"))
                    f.configure(underline=True)
                    label.configure(font=f)
                    label.bind("<Button-1>", logout)

                if j == 2:
                    label = tk.Label(master=frame, text="  Rezervační Systém",
                                     font=("Arial", 15))
                    label.pack()
                if j == 4:
                    frame.grid(row=i, column=j, sticky="NE")
                    label = tk.Label(frame, text="Přihlášen: správce")
                    label.pack()
        else:
            for j in range(5):
                frame = tk.Frame(
                    master=window,
                )
                frame.grid(row=i, column=j, padx=5, pady=5)

                label = tk.Label(master=frame)
                label.pack(padx=5, pady=5)



def screen_reservations():
    wrapper = tk.LabelFrame(window)
    canvas = tk.Canvas(wrapper)
    canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    scroll = tk.Scrollbar(wrapper, orient="vertical", command=canvas.yview)
    scroll.pack(side=tk.RIGHT, fill="y")

    canvas.configure(yscrollcommand=scroll.set)

    canvas.bind("<Configure>", lambda e: canvas.configure(
        scrollregion=canvas.bbox("all")))

    frame = tk.Frame(canvas)
    canvas_frame = canvas.create_window((0,0), window=frame, anchor="nw")


    def FrameWidth(event):
        canvas_width = event.width
        canvas.itemconfig(canvas_frame, width=canvas_width)

    def OnFrameConfigure(event):
        canvas.configure(scrollregion=canvas.bbox("all"))

    frame.bind("<Configure>", OnFrameConfigure)
    canvas.bind('<Configure>', FrameWidth)

    wrapper.pack(anchor=tk.W, fill=tk.X, pady=30, padx=30)

    def cancel_reservation(eventid, event):
        functions.remove_reservation(CURRENT_USER, eventid)
        set_screen(screen_reservations)

    def remove_event(eventid, event):
        functions.remove_event(eventid)
        set_screen(screen_reservations)

    def screen_attend(eventid, event):
        newWindow = tk.Toplevel(window)
        newWindow.geometry(f"200x200+{window.winfo_x() + round(window.winfo_width()//2.3)}+{window.winfo_y() + window.winfo_height()//5}")
        lstbox = tk.Listbox(newWindow)
        lstbox.pack()
        for userid in functions.reservations_event.get(eventid):
            usr = functions.users.get(userid)
            lstbox.insert(tk.END, f"{usr.get('firstname')}"
                                  f" {usr.get('surname')}")


    subframe1 = tk.Frame(frame)
    subframe2 = tk.Frame(frame)
    if CURRENT_USER == 1:
        for eveid, eve in functions.events.items():
            name = eve.get("name")
            date = eve.get("date")

            interactive_frame = tk.Frame(subframe2)
            interactive_frame.pack(anchor=tk.E)

            desc_lbl = tk.Label(subframe1, text=f"{name} - {date}")

            attend_lbl = tk.Label(interactive_frame, text="zobrazit účastníky",
                                  fg="dodger blue")
            f = font.Font(attend_lbl, attend_lbl.cget("font"))
            f.configure(underline=True)
            attend_lbl.configure(font=f)
            attend_lbl.bind("<Button-1>",
                           partial(screen_attend, eveid))

            cancel_lbl = tk.Label(interactive_frame, text="smazat událost",
                                  fg="dodger blue")

            f = font.Font(cancel_lbl, cancel_lbl.cget("font"))
            f.configure(underline=True)
            cancel_lbl.configure(font=f)
            cancel_lbl.bind("<Button-1>",
                            partial(remove_event, eveid))

            desc_lbl.pack(anchor=tk.W, padx=10)
            attend_lbl.pack(anchor=tk.E, side=tk.LEFT, padx=10)
            cancel_lbl.pack(anchor=tk.E, side=tk.LEFT,  padx=10)


    else:
        for reservation in functions.get_user_reservations(CURRENT_USER):

            event = functions.events.get(reservation)
            name = event.get("name")
            date = event.get("date")


            desc_lbl = tk.Label(subframe1, text=f"{name} - {date}")
            cancel_lbl = tk.Label(subframe2, text="zrušit rezervaci",
                                  fg="dodger blue")

            f = font.Font(cancel_lbl, cancel_lbl.cget("font"))
            f.configure(underline=True)
            cancel_lbl.configure(font=f)
            cancel_lbl.bind("<Button-1>", partial(cancel_reservation,reservation))

            desc_lbl.pack(anchor=tk.W, padx=10)
            cancel_lbl.pack(anchor=tk.E, padx=10)

    subframe1.grid(row=0, column=0, sticky="nsew")
    subframe2.grid(row=0, column=1, sticky="nsew")

    frame.grid_columnconfigure(0, weight=1, uniform="group1")
    frame.grid_columnconfigure(1, weight=1, uniform="group1")
    frame.grid_rowconfigure(0, weight=1)


    def choose_backscreen():
        if CURRENT_USER == 1:
            set_screen(screen_admin)
        else:
            set_screen(screen_user)

    back_btn = tk.Button(window, text="Zpět",
                         command=choose_backscreen)
    back_btn.pack(anchor=tk.CENTER)


def screen_create_event():
    frame = tk.Frame(window, bd=5, highlightbackground="black",
                     highlightthickness=1)
    frame.pack(anchor=tk.W, padx=30, pady=30, fill=tk.BOTH)

    subframe1 = tk.Frame(frame)

    name_lbl = tk.Label(subframe1, text="Název události:")
    name_lbl.pack(anchor=tk.W)

    name_entry = tk.Entry(subframe1)
    name_entry.pack(anchor=tk.W, padx=15)

    street_lbl = tk.Label(subframe1, text="Ulice:")
    street_lbl.pack(anchor=tk.W)

    street_entry = tk.Entry(subframe1)
    street_entry.pack(anchor=tk.W, padx=15)

    city_label = tk.Label(subframe1, text="Město:")
    city_label.pack(anchor=tk.W)

    city_entry = tk.Entry(subframe1)
    city_entry.pack(anchor=tk.W, padx=15)

    zip_label = tk.Label(subframe1, text="PSČ:")
    zip_label.pack(anchor=tk.W)

    zip_entry = tk.Entry(subframe1)
    zip_entry.pack(anchor=tk.W, padx=15)

    back_btn = tk.Button(subframe1, text="Zpět",
                         command=lambda:set_screen(screen_admin))
    back_btn.pack(anchor=tk.SW, padx=30, pady=20)


    subframe2 = tk.Frame(frame)

    date_label = tk.Label(subframe2, text="Termín konání: ")
    date_label.pack(anchor=tk.W)

    date = tkcalendar.DateEntry(subframe2, bg="darkblue", fg="white",
                                year=2021, mindate=datetime.date.today())
    date.pack(anchor=tk.CENTER, padx=15)

    capacity_label = tk.Label(subframe2, text="Kapacita:")
    capacity_label.pack(anchor=tk.W)

    capacity_entry = tk.Entry(subframe2, width=6)
    capacity_entry.pack(anchor=tk.CENTER)

    desc_label = tk.Label(subframe2, text="Popis události:")
    desc_label.pack(anchor=tk.W)

    desc_field = tk.Text(subframe2, height=3)
    desc_field.pack(anchor=tk.W, padx=15)

    def create_event():
        functions.create_event(name_entry.get(),desc_field.get("1.0", tk.END),
                               date.get_date(), int(capacity_entry.get()), (street_entry.get(), city_entry.get(), zip_entry.get()))
        set_screen(screen_user)

    create_btn = tk.Button(subframe2, text="Vytvořit", command=create_event)
    create_btn.pack(anchor=tk.SE, padx=30, pady=20)

    subframe1.grid(row=0, column=0, sticky="nsew")
    subframe2.grid(row=0, column=1, sticky="nsew")

    frame.grid_columnconfigure(0, weight=1, uniform="group1")
    frame.grid_columnconfigure(1, weight=1, uniform="group1")
    frame.grid_rowconfigure(0, weight=1)

def screen_create_reservation():
    frame = tk.Frame(window, bd=5, highlightbackground="black",
                     highlightthickness=1)
    frame.pack(anchor=tk.W, padx=30, pady=30, fill=tk.BOTH)

    dummy = {"name": "", "capacity": 0, "attending": 0,
             "placeid": 0}

    choices = {}
    choices["Vyberte událost"] = dummy

    for eveid, eve in functions.events.items():
        choices[eve.get("name")] = eveid


    tkvar = tk.StringVar(window)
    tkvar.set(list(choices.keys())[0])

    event = dummy


    place = tk.StringVar()

    place.set(f"")

    desc = tk.StringVar()
    desc.set("")

    capacity = tk.StringVar()
    capacity.set(f'')

    capacity_full = tk.StringVar()
    capacity_full.set("")


    date = tk.StringVar()
    date.set("")

    dropdown = tk.OptionMenu(frame, tkvar, *choices)
    dropdown.pack(anchor=tk.CENTER)



    gridframe = tk.Frame(frame)
    gridframe.pack(anchor=tk.W, fill=tk.BOTH)

    subframe1 = tk.Frame(gridframe)

    place_desc = tk.Label(subframe1, text="Místo konání:")
    place_desc.pack(anchor=tk.W)

    place_label = tk.Label(subframe1, textvariable=place)
    place_label.pack(anchor=tk.W, padx=15)


    desc_desc = tk.Label(subframe1, text="Popis události:")
    desc_desc.pack(anchor=tk.W)

    desc_label = scrolledtext.ScrolledText(subframe1, width=30, heigh=10)
    desc_label.insert(tk.END, desc.get())
    desc_label.pack(anchor=tk.W, padx=15)


    subframe2 = tk.Frame(gridframe)

    date_desc = tk.Label(subframe2, text="Termín události:")
    date_desc.pack(anchor=tk.W)

    date_label = tk.Label(subframe2, textvariable=date)
    date_label.pack(anchor=tk.W)

    capacity_desc = tk.Label(subframe2, text="Kapacita:")
    capacity_desc.pack(anchor=tk.W)

    capacity_label = tk.Label(subframe2, textvariable=capacity)
    capacity_label.pack(anchor=tk.W)

    full_label = tk.Label(subframe2, textvariable=capacity_full)
    full_label.pack(anchor=tk.W)
    full_label.config(fg="red")



    subframe1.grid(row=0, column=0, sticky="nsew")
    subframe2.grid(row=0, column=1, sticky="nsew")

    gridframe.grid_columnconfigure(0, weight=1, uniform="group1")
    gridframe.grid_columnconfigure(1, weight=1, uniform="group1")
    gridframe.grid_rowconfigure(0, weight=1)

    gridframe2 = tk.Frame(frame)
    gridframe2.pack(anchor=tk.W, fill=tk.BOTH)

    def create_reservation(eventid):
        functions.create_reservation(CURRENT_USER, eventid)
        set_screen(screen_user)


    create_btn_frame = tk.Frame(gridframe2)
    create_btn = tk.Button(create_btn_frame, text="Potvrdit",
                           state=tk.DISABLED,
                           command=lambda: create_reservation(choices.get(tkvar.get())))
    create_btn.pack(anchor=tk.E, padx=30, pady=20)

    back_btn_frame = tk.Frame(gridframe2)
    back_btn = tk.Button(back_btn_frame, text="Zpět",
                         command=lambda: set_screen(screen_user))
    back_btn.pack(anchor=tk.W, padx=30, pady=20)

    create_btn_frame.grid(row=0, column=1, sticky="nsew")
    back_btn_frame.grid(row=0, column=0, sticky="nsew")

    gridframe2.grid_columnconfigure(0, weight=1, uniform="group1")
    gridframe2.grid_columnconfigure(1, weight=1, uniform="group1")
    gridframe2.grid_rowconfigure(0, weight=1)

    def change_dropdown(*args):
        event = functions.events[choices.get(tkvar.get())]

        date.set(event["date"])

        placevar = functions.places[event["placeid"]]
        place.set(f"{placevar.get('street')}, {placevar.get('zipcode')},"
                  f" {placevar.get('city')}")

        desc.set(textwrap.fill(event["description"], width=30))
        desc_label.delete('1.0', tk.END)
        desc_label.insert(tk.END, desc.get())

        capacity.set(f'{event["attending"]}/{event["capacity"]}')
        if event["attending"] >= event["capacity"]:
            capacity_full.set("Kapacita události je zaplněna")
            create_btn.config(state=tk.DISABLED)
        else:
            capacity_full.set("")
            create_btn.config(state=tk.NORMAL)

    tkvar.trace('w', change_dropdown)


screen_intro()

window.mainloop()

